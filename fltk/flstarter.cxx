// generated by Fast Light User Interface Designer (fluid) version 1.0304

#include "flstarter.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <dirent.h>
#include <limits.h>
static char variable[PATH_MAX]; 
static int test = 14; 

Fl_Double_Window *window1=(Fl_Double_Window *)0;

Fl_Input *inputbox1=(Fl_Input *)0;

static void cb_OK(Fl_Button*, void*) {
  printf( "VALUE %s\n" , inputbox1->value() );
}

Fl_Double_Window* make_window() {
  { window1 = new Fl_Double_Window(450, 360);
    { new Fl_Clock(125, 30, 175, 155);
    } // Fl_Clock* o
    { inputbox1 = new Fl_Input(95, 230, 300, 25, "Input box");
    } // Fl_Input* inputbox1
    { Fl_Button* o = new Fl_Button(95, 255, 300, 25, "OK");
      o->callback((Fl_Callback*)cb_OK);
    } // Fl_Button* o
    window1->end();
  } // Fl_Double_Window* window1
  return window1;
}

int main( int argc, char *argv[]) {
  printf( " Hello World \n Use compilation in BSD and Linux: pwd ; c++ -lm   -I/usr/X11R7/include/ -I/usr/pkg/include -I /usr/pkg/include/    -L/usr/pkg/lib  -L/usr/X11R7/lib -lX11 -I /usr/X11R7/include/   -lfltk   fltk/flstarter.cxx          -o   bin/flstarter  \n "); 
  
    make_window();
    
  
   /// here change your GUI ...
    
    
    window1->show();
    
    Fl::run();
}
